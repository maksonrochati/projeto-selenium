package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class SecretaPage extends BasePage{

    // Construtor
    public SecretaPage(WebDriver navegador) {
        super(navegador);
    }

    public MePage clicarMe(){
        // Clicar no elemento com class:'me'
        //navegador.findElement(By.className("me")).click();
        //navegador.findElement(By.xpath("//li/a[@class='me']")).click();

        // Clica no link através do texto na tela 'Hi, Julio'
        navegador.findElement(By.linkText("Hi, Julio")).click();

        return new MePage(navegador);
    }
}
