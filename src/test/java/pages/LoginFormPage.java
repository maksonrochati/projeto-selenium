package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class LoginFormPage extends BasePage{

    // Construtor
    public LoginFormPage(WebDriver navegador) {
        super(navegador);
    }

    public LoginFormPage digitarLogin(String login){

        // Identificando o formulário de Login com  id:'signinbox'
        // Digitar no campo com name:'Login' que está dentro do formulario de  id:'signinbox' o texto 'julio0001'
        navegador.findElement(By.id("signinbox")).findElement(By.name("login")).sendKeys(login);

        return this;
    }

    public LoginFormPage digitarSenha(String senha){

        // Identificando o formulário de Login com  id:'signinbox'
        // Digitar no campo com name:'password' que está dentro do formulario de  id:'signinbox' o texto '123456'
        navegador.findElement(By.id("signinbox")).findElement(By.name("password")).sendKeys(senha);

        return this;
    }

    public SecretaPage clicarSignIn(){
        // Clicar no link com nome 'SIGN IN'
        navegador.findElement(By.linkText("SIGN IN")).click();

        return new SecretaPage(navegador);
    }

    public SecretaPage fazerlogin(String login, String senha){
        digitarLogin(login);
        digitarSenha(senha);
       // clicarSignIn();

        //return new SecretaPage(navegador);
        return clicarSignIn();
    }
}
