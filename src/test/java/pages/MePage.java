package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class MePage extends BasePage{

    // Construtor
    public MePage(WebDriver navegador) {
        super(navegador);
    }

    public MePage clicarNaAbaMoreDataAboutYou(){
        // Clicar no link com texto 'MORE DATA ABOUT YOU'
        navegador.findElement(By.linkText("MORE DATA ABOUT YOU")).click();

        return this;
    }

    public AddContactPage clicarNoBotaoAddMoreDataAboutYou(){
        // Clicar no botão '+ ADD MORE DATA' através xpath:'//div[@id='moredata']//button[text()='+ Add more data']'
        navegador.findElement(By.xpath("//div[@id='moredata']//button[text()=\"+ Add more data\"]")).click();

        return new AddContactPage(navegador);

    }
}
