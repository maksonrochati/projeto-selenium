package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

public class AddContactPage extends BasePage {

    // Construtor
    public AddContactPage(WebDriver navegador) {
        super(navegador);
    }

    public AddContactPage escolherTipoDeContato(String tipo) {

        // Identificar a popup onde está o formulário de id:'addmoredata'
        // na combo de name:'type' escolher a opção 'Phone'
        WebElement campoType = navegador.findElement(By.id("addmoredata")).findElement(By.name("type"));
        new Select(campoType).selectByVisibleText(tipo);

        return this;
    }

    public AddContactPage digitarContato(String contato) {

        // No campo de name:'contact' digitar '+5511999992222'
        navegador.findElement(By.id("addmoredata")).findElement(By.name("contact")).sendKeys(contato);

        return this;
    }

    public MePage clicarSalvar() {

        // Clicar no link de text 'SAVE' que está na popup
        navegador.findElement(By.id("addmoredata")).findElement(By.linkText("SAVE")).click();

        return new MePage(navegador);
    }

    public MePage adicionarContato(String tipo, String contato) {
        escolherTipoDeContato(tipo);
        digitarContato(contato);
        clicarSalvar();

        return new MePage(navegador);
    }

}
