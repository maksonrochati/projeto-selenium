package suporte;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;

public class Web {

    private static WebDriver navegador;

    public static final String USERNAME = "maksonrocha1";
    public static final String AUTOMATE_KEY = "eRfexrmspYee1y1v6yoz";
    public static final String URLBrowserStack = "https://" + USERNAME + ":" + AUTOMATE_KEY + "@hub-cloud.browserstack.com/wd/hub";


    public static WebDriver createChrome() {

        // Variavel com o caminho do driver dentro do projeto.
        String urlDriverChrome = System.getProperty("user.dir") + "\\Driver\\chromedriver.exe";
        //String urlDriverFireFox = System.getProperty("user.dir") + "\\Driver\\geckodriver.exe";
        //String urlDriverEdger = System.getProperty("user.dir") + "\\Driver\\msedgedriver.exe";

        // Abre o navegador.
        System.setProperty("webdriver.chrome.driver", urlDriverChrome);
        //System.setProperty("webdriver.gecko.driver", urlDriverFireFox);
        //System.setProperty("webdriver.edge.driver", urlDriverEdger);

        navegador = new ChromeDriver();
        //navegador = new FirefoxDriver();
        //navegador = new EdgeDriver();

        // Espera implicita
        navegador.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

        // Maximiza o navegador
        //navegador.manage().window().maximize();

        // Define o tamanho da tela do navegador
        //navegador.manage().window().setSize(new Dimension(1366,700));

        // Acessa o site a ser testado
        navegador.get("http://www.juliodelima.com.br/taskit");

        return navegador;
    }

    public static WebDriver createBrowserStack(){

        DesiredCapabilities caps = new DesiredCapabilities();
        caps.setCapability("browser", "Chrome");
        caps.setCapability("browser_version", "79.0");
        caps.setCapability("os", "Windows");
        caps.setCapability("os_version", "7");
        caps.setCapability("resolution", "1024x768");
        caps.setCapability("name", "Bstack-[Java] Sample Test");

        WebDriver navegadorRemote =  null;

        try {
             navegadorRemote = new RemoteWebDriver(new URL(URLBrowserStack), caps);

            // Espera implicita
            navegadorRemote.manage().timeouts().implicitlyWait(12, TimeUnit.SECONDS);

            // Acessa o site a ser testado
            navegadorRemote.get("http://www.juliodelima.com.br/taskit");

        } catch (MalformedURLException e) {
            System.out.println("Houveram problemas com a URL: " + e.getMessage());
        }

        return navegadorRemote;
    }
}
