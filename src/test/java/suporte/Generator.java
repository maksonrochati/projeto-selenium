package suporte;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;

public class Generator {

    public static String dataHoraParaArquivo(){

        // Propriedade que informa o formato de data e hora para arquivo
        Timestamp ts = new Timestamp(System.currentTimeMillis());

        // Retorno da data e hora no formato que será usado
        return new SimpleDateFormat("yyyyMMddhhmmss").format(ts);

    }
}
