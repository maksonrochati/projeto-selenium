package suporte;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;

import java.io.File;
import java.io.IOException;

public class Screenshot {

    public static void tirar(WebDriver navegador, String arquivo){

        // Propriendade que informa o tipo de saida do arquivo
        File sreenshot = ((TakesScreenshot)navegador).getScreenshotAs(OutputType.FILE);

        try {
            // Copia o arquivo para pasta que será informada ao chamar o método tirar()
            FileUtils.copyFile(sreenshot, new File(arquivo));
        } catch (IOException ex) {
            System.out.println("Houveram problemas ao copiar o arquivo para a pasta" + ex.getMessage());
        }
    }

    public static String localDoArquivo(String arquivo){

        // Chamada da classe Screenshot com o método tirar()
        String pastaArquivos = System.getProperty("user.dir") + "\\ImgScreenshot\\";

        // Pega a pasta onde será salvo o arquivo e concatena com os dados que serão salvos no arquivo
        String dadosArquivo = pastaArquivos + Generator.dataHoraParaArquivo() + arquivo + ".png";

        return dadosArquivo;
    }
}
