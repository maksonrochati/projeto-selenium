package tests;


import org.easetech.easytest.annotation.DataLoader;
import org.easetech.easytest.annotation.Param;
import org.easetech.easytest.runner.DataDrivenTestRunner;
import org.junit.After;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.WebDriver;
import pages.LoginPage;
import suporte.Web;

import static org.junit.Assert.assertEquals;


@RunWith(DataDrivenTestRunner.class)
@DataLoader(filePaths = "InformacoesUsuarioPageObjectsTestData.csv")
public class InformacoesUsuarioPageObjectsTest {

    private WebDriver navegador;

    @Before
    public void setUp(){
        // Driver que está na maquina
        navegador = Web.createChrome();

        // Remote driver do BrowserStack
        //navegador = Web.createBrowserStack();

    }

    @Test
    public void testAdicionarUmaInformacaoAdicionaldoUsuario(@Param(name = "login") String login,
                                                             @Param(name = "senha") String senha,
                                                             @Param(name ="tipo") String tipo,
                                                             @Param(name = "contato") String contato,
                                                             @Param(name = "mensagem") String mensagemEsperada
    ) {

        String textoToast = new LoginPage(navegador)
                .clicarSignIn()
                .fazerlogin(login, senha)
                .clicarMe()
                .clicarNaAbaMoreDataAboutYou()
                .clicarNoBotaoAddMoreDataAboutYou()
                .adicionarContato(tipo, contato)
                .capturarTextoDoToast();

        assertEquals(mensagemEsperada, textoToast);

    }

    @After
    public void tearDown(){
        navegador.quit();
    }
}
