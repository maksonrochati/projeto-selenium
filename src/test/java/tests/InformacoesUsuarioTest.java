package tests;

import static org.junit.Assert.*;

import org.easetech.easytest.annotation.DataLoader;
import org.easetech.easytest.annotation.Param;
import org.easetech.easytest.runner.DataDrivenTestRunner;
import org.junit.*;
import org.junit.rules.TestName;
import org.junit.runner.RunWith;
import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import suporte.Generator;
import suporte.Screenshot;

import java.util.concurrent.TimeUnit;

// Anotação do easyTest com a classe da biblioteca DataDrivenTestRunner
@RunWith(DataDrivenTestRunner.class)
// Aponta para os arquivos csv que guarda os dados utilizados no teste
@DataLoader(filePaths = "InformacoesUsuarioTestData.csv")

public class InformacoesUsuarioTest {

    private WebDriver navegador;

    // Propriedade que habilita o uso do nome da classe no screenshot
    @Rule
    public TestName nomeDaClasse = new TestName();

    @Before
    public void setUp() {

        // Variavel com o caminho do driver dentro do projeto.
        //String urlDriverChrome = System.getProperty("user.dir") + "\\Driver\\chromedriver.exe";
        String urlDriverChrome = System.getProperty("user.dir") + "\\Driver\\geckodriver.exe";
        //String urlDriverEdger = System.getProperty("user.dir") + "\\Driver\\msedgedriver.exe";

        // Abre o navegador.
        //System.setProperty("webdriver.chrome.driver", urlDriverChrome);
        System.setProperty("webdriver.gecko.driver", urlDriverChrome);
        //System.setProperty("webdriver.edge.driver", urlDriverEdger);

        //navegador = new ChromeDriver();
        navegador = new FirefoxDriver();
        //navegador = new EdgeDriver();

        // Maximiza o navegador
        //navegador.manage().window().maximize();

        // Espera implicita
        navegador.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);

        // Define o tamanho da tela do navegador
        //navegador.manage().window().setSize(new Dimension(1349,624));

        navegador.get("http://www.juliodelima.com.br/taskit");


        // Faz o login na aplicação

        // Clicar no link com texto 'Sign in'
        navegador.findElement(By.linkText("Sign in")).click();

        // Identificando o formulário de Login com  id:'signinbox'
        WebElement formLogin = navegador.findElement(By.id("signinbox"));
        //navegador.findElement(By.id("signinbox")).findElement(By.name("login")).sendKeys("julio0001");

        // Digitar no campo com name:'Login' que está dentro do formulario de  id:'signinbox' o texto 'julio0001'
        formLogin.findElement(By.name("login")).sendKeys("julio0001");

        // Digitar no campo com name:'password' que está dentro do formulario de  id:'signinbox' o texto '123456'
        formLogin.findElement(By.name("password")).sendKeys("123456");

        // Clicar no link com nome 'SIGN IN'
        navegador.findElement(By.linkText("SIGN IN")).click();

        // Clicar no elemento com class:'me'
        //navegador.findElement(By.className("me")).click();
        //navegador.findElement(By.xpath("//li/a[@class='me']")).click();
        navegador.findElement(By.linkText("Hi, Julio")).click();

        // Clicar no link com texto 'MORE DATA ABOUT YOU'
        navegador.findElement(By.linkText("MORE DATA ABOUT YOU")).click();
    }



    @Test
    public void testAdicionarUmaInformacaoAdicionaldoUsuario(@Param(name="tipo")String tipo, @Param(name="telefone")String telefone, @Param(name="mensagem")String mensagemEsperada) {

        // Clicar no botão '+ ADD MORE DATA' através xpath:'//div[@id='moredata']//button[text()='+ Add more data']'
        navegador.findElement(By.xpath("//div[@id='moredata']//button[text()=\"+ Add more data\"]")).click();

        // Identificar a popup onde está o formulário de id:'addmoredata'
        WebElement popup = navegador.findElement(By.id("addmoredata"));

        // na combo de name:'type' escolher a opção 'Phone'
        WebElement campoType = popup.findElement(By.name("type"));
        new Select(campoType).selectByVisibleText(tipo);

        // No campo de name:'contact' digitar '+5511999992222'
        popup.findElement(By.name("contact")).sendKeys(telefone);

        // Clicar no link de text 'SAVE' que está na popup
        popup.findElement(By.linkText("SAVE")).click();

        // Na mensagem de id:'toast-container' validar que o texto é 'Your contact has been added!'
        WebElement msg = navegador.findElement(By.id("toast-container"));
        String mensagem = msg.getText();
        assertEquals(mensagemEsperada, mensagem);

        // Aguardar até 10s para que a janela desapareça
//        WebDriverWait aguardar = new WebDriverWait(navegador, 10);
//        aguardar.until(ExpectedConditions.stalenessOf(msg));
    }

    @Ignore
    @Test
    public void testRemoverContatoDoUsuario(){

        // Contato para remover '+551133334444'
        // Localizar contato pelo span pelo texto:'+551133334444' e buscar o próximo link atráves do xpath:'//span[text()='+551133334444']/following-sibling::a'
        // Clicar no elemento pelo xpath:'//span[text()='+551133334444']/following-sibling::a'
        navegador.findElement(By.xpath("//span[text()='+551133334444']/following-sibling::a")).click();

        // Confirmar a janela java script
        navegador.switchTo().alert().accept();

        // Validar que a mensagem apresentada foi 'Rest in peace, dear phone!'
        WebElement msg = navegador.findElement(By.id("toast-container"));
        String mensagem = msg.getText();
        assertEquals("Rest in peace, dear phone!", mensagem);


        // Parte do Screenshot

        // Chamada da classe Screenshot com o método tirar()
        //String pastaArquivos = System.getProperty("user.dir") + "\\ImgScreenshot\\";

        // Pega a pasta onde será salvo o arquivo e concatena com os dados que serão salvos no arquivo
        //String dadosArquivo = pastaArquivos + Generator.dataHoraParaArquivo() + nomeDaClasse.getMethodName() + ".png";

        // Trazendo o método de localDoArquivo() da classe Screenshot e colocando na váriavel localArquivo
        String localArquivo =  Screenshot.localDoArquivo(nomeDaClasse.getMethodName());

        // Chamada da classe Screeshot com o método tirar()
        Screenshot.tirar(navegador, localArquivo);


        // Aguardar até 10s para que a janela desapareça
        WebDriverWait aguardar = new WebDriverWait(navegador, 10);
        aguardar.until(ExpectedConditions.stalenessOf(msg));

        // Clicar no link com texto 'Logout'
        navegador.findElement(By.linkText("Logout")).click();

    }

    @After
    public void tearDown() {

        // Fechar o navegador
        //navegador.quit();
    }
}
